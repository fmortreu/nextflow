profiles {
  docker {
    docker.temp = 'auto'
    docker.enabled = true
    process {
      withName: sort_bam {
        container = "lbmc/samtools:1.7"
        cpus = 1
      }
      withName: counting {
        container = "lbmc/subread:1.6.4"
        cpus = 1
      }
    }
  }
  singularity {
    singularity.enabled = true
    singularity.cacheDir = "./bin/"
    process {
      withName: sort_bam {
        container = "lbmc/samtools:1.7"
        cpus = 1
      }
      withName: counting {
        container = "lbmc/subread:1.6.4"
        cpus = 1
      }
    }
  }
  psmn{
    process{
      withName: sort_bam {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = 'monointeldeb128,monointeldeb48,h48-E5-2670deb128,h6-E5-2667v4deb128'
      }
      withName: counting {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "subread/1.6.4"
        executor = "sge"
        clusterOptions = "-cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = 'monointeldeb128,monointeldeb48,h48-E5-2670deb128,h6-E5-2667v4deb128'
      }
    }
  }
  ccin2p3 {
    singularity.enabled = true
    singularity.cacheDir = "$baseDir/.singularity_in2p3/"
    singularity.runOptions = "--bind /pbs,/sps,/scratch"
    process{
      withName: sort_bam {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n\
        "
        cpus = 1
        queue = 'huge'
      }
      withName: counting {
        container = "lbmc/subread:1.6.4"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n\
        "
        cpus = 1
        queue = 'huge'
      }
    }
  }
}
